part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;

  User(
      {this.id,
      this.name,
      this.email,
      this.address,
      this.houseNumber,
      this.phoneNumber,
      this.city,
      this.picturePath});

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, name, email, address, houseNumber, phoneNumber, city, picturePath];
}

User dummyUser = User(
    id: 1,
    name: 'Kim Dahyun',
    address: "Jalan Perjuangan",
    city: 'Banjarbaru',
    houseNumber: '16',
    phoneNumber: '0829929292',
    email: 'kimdahyun@gmailom',
    picturePath:
        "https://pbs.twimg.com/profile_images/1262985081787830273/N_4oyrm6_400x400.jpg");
