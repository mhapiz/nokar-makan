import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

part 'food.dart';
part 'transaction.dart';
part 'user.dart';
part 'api_return_value.dart';
