part of 'models.dart';

enum FoodType { new_food, popular, recommended }

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;
  final List<FoodType> types;

  Food(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.ingredients,
      this.price,
      this.rate,
      this.types = const []});

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, picturePath, name, description, ingredients, price, rate];
}

List<Food> dummyFoods = [
  Food(
      id: 1,
      picturePath:
          "https://images.unsplash.com/photo-1568901346375-23c9450c58cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=902&q=80",
      name: "Burger Sultan Bozz",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Roti, Sayur, Daging, Telur",
      price: 14000,
      rate: 4,
      types: [FoodType.new_food, FoodType.popular]),
  Food(
      id: 2,
      picturePath:
          "https://images.unsplash.com/photo-1515683359900-6922e4964be1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      name: "Salad Morning Jos",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Roti, Sayur, Daging, Telur",
      price: 14350,
      rate: 3.8),
  Food(
      id: 3,
      picturePath:
          "https://images.unsplash.com/photo-1593870682262-8c9f6a9bb225?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80",
      name: "Usus Sapi",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Usus Aja",
      price: 14000,
      rate: 5,
      types: [FoodType.recommended, FoodType.popular]),
  Food(
      id: 4,
      picturePath:
          "https://images.unsplash.com/photo-1568901346375-23c9450c58cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=902&q=80",
      name: "Burger Sultan Bozz",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Roti, Sayur, Daging, Telur",
      price: 14000,
      rate: 4,
      types: [FoodType.popular]),
  Food(
      id: 5,
      picturePath:
          "https://images.unsplash.com/photo-1515683359900-6922e4964be1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      name: "Salad Morning Jos",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Roti, Sayur, Daging, Telur",
      price: 14000,
      rate: 3.8),
  Food(
      id: 6,
      picturePath:
          "https://images.unsplash.com/photo-1593870682262-8c9f6a9bb225?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80",
      name: "Usus Sapi",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Usus Aja",
      price: 14000,
      rate: 5,
      types: [FoodType.new_food, FoodType.popular, FoodType.recommended]),
  Food(
      id: 7,
      picturePath:
          "https://images.unsplash.com/photo-1568901346375-23c9450c58cd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=902&q=80",
      name: "Burger Sultan Bozz",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Roti, Sayur, Daging, Telur",
      price: 14000,
      rate: 4),
  Food(
      id: 8,
      picturePath:
          "https://images.unsplash.com/photo-1515683359900-6922e4964be1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
      name: "Salad Morning Jos",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Roti, Sayur, Daging, Telur",
      price: 14000,
      rate: 3.8,
      types: [FoodType.recommended]),
  Food(
      id: 9,
      picturePath:
          "https://images.unsplash.com/photo-1593870682262-8c9f6a9bb225?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80",
      name: "Usus Sapi",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
      ingredients: "Usus Aja",
      price: 14000,
      rate: 5),
];
