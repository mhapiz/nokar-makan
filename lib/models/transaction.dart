part of 'models.dart';

enum TransactionStatus { delivered, on_delivery, pending, cancelled }

class Transaction extends Equatable {
  final int id;
  final Food food;
  final int quantity;
  final int total;
  final DateTime dateTime;
  final TransactionStatus status;
  final User user;

  Transaction({
    this.id,
    this.food,
    this.quantity,
    this.total,
    this.dateTime,
    this.status,
    this.user,
  });

  Transaction copyWith({
    int id,
    Food food,
    int quantity,
    int total,
    DateTime dateTime,
    TransactionStatus status,
    User user,
  }) {
    return Transaction(
        id: id ?? this.id,
        food: food ?? this.food,
        quantity: quantity ?? this.quantity,
        total: total ?? this.total,
        dateTime: dateTime ?? this.dateTime,
        status: status ?? this.status,
        user: user ?? this.user);
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        id,
        food,
        quantity,
        total,
        dateTime,
        status,
        user,
      ];
}

List<Transaction> dummyTransactions = [
  Transaction(
      id: 1,
      food: dummyFoods[1],
      quantity: 4,
      total: (dummyFoods[1].price * 10 * 1.1).round() + 5000,
      dateTime: DateTime.now(),
      status: TransactionStatus.on_delivery,
      user: dummyUser),
  Transaction(
      id: 2,
      food: dummyFoods[2],
      quantity: 2,
      total: (dummyFoods[2].price * 2 * 1.1).round() + 5000,
      dateTime: DateTime.now(),
      status: TransactionStatus.on_delivery,
      user: dummyUser),
  Transaction(
      id: 3,
      food: dummyFoods[3],
      quantity: 3,
      total: (dummyFoods[3].price * 3 * 1.1).round() + 5000,
      dateTime: DateTime.now(),
      status: TransactionStatus.on_delivery,
      user: dummyUser),
  Transaction(
      id: 4,
      food: dummyFoods[0],
      quantity: 6,
      total: (dummyFoods[0].price * 6 * 1.1).round() + 5000,
      dateTime: DateTime.now(),
      status: TransactionStatus.cancelled,
      user: dummyUser),
];
