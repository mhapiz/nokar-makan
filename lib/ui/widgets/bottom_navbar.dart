part of 'widgets.dart';

class BottomNavbar extends StatelessWidget {
  final int selectedIndex;
  final Function(int index) onTap;

  BottomNavbar({this.selectedIndex = 2, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: double.infinity,
      color: "FFFFFF".toColor(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap(0);
              }
            },
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        "assets/home" +
                            ((selectedIndex == 0) ? 'Active.png' : '.png'),
                      ),
                      fit: BoxFit.cover)),
            ),
          ),
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap(1);
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 90),
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        "assets/basket" +
                            ((selectedIndex == 1) ? 'Active.png' : '.png'),
                      ),
                      fit: BoxFit.cover)),
            ),
          ),
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap(2);
              }
            },
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        "assets/profile" +
                            ((selectedIndex == 2) ? 'Active.png' : '.png'),
                      ),
                      fit: BoxFit.cover)),
            ),
          ),
        ],
      ),
    );
  }
}
