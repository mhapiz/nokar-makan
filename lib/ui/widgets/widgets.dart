import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:nokar_makan/models/models.dart';
import 'package:nokar_makan/shared/shared.dart';
import 'package:supercharged/supercharged.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

part 'bottom_navbar.dart';
part 'food_rating.dart';
part 'custom_tabbar.dart';
part 'food_list_item.dart';
part 'order_list_item.dart';
