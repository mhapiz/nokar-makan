part of 'pages.dart';

class OrderHistoryPage extends StatefulWidget {
  @override
  _OrderHistoryPageState createState() => _OrderHistoryPageState();
}

class _OrderHistoryPageState extends State<OrderHistoryPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransactionCubit, TransactionState>(builder: (_, state) {
      if (state is TransactionLoaded) {
        if (state.transactions.length == 0) {
          return IllustrationPage(
            title: "Nah Lapar Kah Kam",
            subtitle: "Sadang ay dah mencari makan",
            picturePath: "assets/love_burger.png",
            buttonTitle1: "Cari Makan",
            buttonTap1: () {},
          );
        } else {
          double listItemWidth =
              MediaQuery.of(context).size.width - 2 * defaultMargin;
          return ListView(
            children: [
              Column(
                children: [
                  //HEADER
                  Container(
                      height: 100,
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: defaultMargin),
                      padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Your Order", style: blackFontStyle1),
                          Text("Wait for the best meal",
                              style: mutedTextStyle.copyWith(
                                  fontWeight: FontWeight.w300))
                        ],
                      )),
                  //BODY
                  Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(children: [
                        CustomTabBar(
                          titles: ["In Progress", "Past Orders"],
                          selectedIndex: selectedIndex,
                          onTap: (index) {
                            setState(() {
                              selectedIndex = index;
                            });
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Builder(builder: (_) {
                          List<Transaction> transaction = (selectedIndex == 0)
                              ? state.transactions
                                  .where((t) =>
                                      t.status ==
                                          TransactionStatus.on_delivery ||
                                      t.status == TransactionStatus.pending)
                                  .toList()
                              : state.transactions
                                  .where((t) =>
                                      t.status == TransactionStatus.delivered ||
                                      t.status == TransactionStatus.cancelled)
                                  .toList();
                          return Column(
                            children: transaction
                                .map((e) => Container(
                                      width: double.infinity,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: defaultMargin),
                                      margin: EdgeInsets.only(bottom: 16),
                                      child: OrderListItem(
                                        transaction: e,
                                        itemWidth: listItemWidth,
                                      ),
                                    ))
                                .toList(),
                          );
                        })
                      ]))
                ],
              )
            ],
          );
        }
      } else {
        return Center(
          child: loadingIndicator,
        );
      }
    });
  }
}
