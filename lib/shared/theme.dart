part of 'shared.dart';

Color kindaBlack = "402E32".toColor();

Color mainColor = "F8CF26".toColor();
Color mutedColor = "8D92A3".toColor();

TextStyle mutedTextStyle = GoogleFonts.poppins().copyWith(color: mutedColor);

TextStyle kindaBlackFontStyle = GoogleFonts.poppins()
    .copyWith(color: "402E32".toColor(), fontWeight: FontWeight.w500);

TextStyle blackFontStyle1 = GoogleFonts.poppins()
    .copyWith(color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500);

TextStyle blackFontStyle2 = GoogleFonts.poppins()
    .copyWith(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500);

TextStyle blackFontStyle3 = GoogleFonts.poppins()
    .copyWith(color: Colors.black, fontWeight: FontWeight.w500);

const double defaultMargin = 24;

Widget loadingIndicator = SpinKitFadingCircle(
  size: 45,
  color: mainColor,
);
